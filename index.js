const server = require("./src/server");

server.listen(3000, () => {
  console.log("Server is running on 3000");
});
