const { Router } = require("express");

const router = Router();
const posts = [
  {
    id: 1,
    content: "This is my post"
  },
  {
    id: 2,
    content: "This is my post 2"
  },
  {
    id: 3,
    content: "This is my post 3"
  },
  {
    id: 4,
    content: "This is my post 4"
  }
];
router.get("/", (req, res) => {
  res.send(posts);
});

module.exports = router;
