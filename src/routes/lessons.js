const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const chechAuth = require("../middleware/check-auth");
const passport = require("passport");
const Lesson = require("../models/lesson");
const jwt = require("jsonwebtoken");

const requireAuth = passport.authenticate("jwt", { session: false });
router.get("/", chechAuth, (req, res, next) => {
  Lesson.find()
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        lessons: docs.map(doc => {
          return {
            name: doc.name,
            price: doc.price,
            teacherName: doc.teacherName,
            location: doc.location,
            timeDuration: doc.timeDuration,
            timeStart: doc.timeStart,
            day: doc.day,
            _id: doc._id,

            request: {
              type: "GET",
              url: "http://localhost:3000/lessons/" + doc._id
            }
          };
        })
      };
      res.status(200).json(response);
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({
        error: error
      });
    });
});
router.get("/:lessonName", (req, res, next) => {
  const nameIn = req.params.lessonName;
  Lesson.find({ name: nameIn })
    .exec()
    .then(docs => {
      console.log("from database", docs);
      if (docs) {
        const response = {
          count: docs.length,
          lessons: docs.map(doc => {
            return {
              name: doc.name,
              price: doc.price,
              teacherName: doc.teacherName,
              location: doc.location,
              timeDuration: doc.timeDuration,
              timeStart: doc.timeStart,
              day: doc.day,
              _id: doc._id,

              request: {
                type: "GET",
                url: "http://localhost:3000/lessons/" + doc._id
              }
            };
          })
        };
        res.status(200).json(response);
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided Name" });
      }
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({ error: error });
    });
});
router.get("/:lessonId", (req, res, next) => {
  const id = req.params.lessonId;
  Lesson.findById(id)
    .exec()
    .then(doc => {
      console.log("from database", doc);
      if (doc) {
        res.status(200).json({
          lesson: doc.lesson,
          teacherName: doc.teacherName,
          location: doc.location,
          timeDuration: doc.timeDuration,
          timeStart: doc.timeStart,
          day: doc.day,
          request: {
            type: "GET",
            descryption: "GET_ALL_LESSONS",
            url: "http://localhost:3000/lessons"
          }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({ error: error });
    });
});

const onlyAdmin = (req, res, next) => {
  const token = req.headers.authorization.split(" ")[1];
  const decoded = jwt.verify(token, process.env.JWT_KEY);
  const user = decoded;
  if (decoded.role == "admin") {
    return next();
  }
  res.status(401).send("Unauthorized");
  return next("Unauthorized");
};

router.post("/", (req, res, next) => {
  const lesson = new Lesson({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    teacherName: req.body.teacherName,
    price: req.body.price,
<<<<<<< HEAD
    location: req.body.location,
    timeDuration: req.body.timeDuration,
    timeStart: req.body.timeStart,
    day: req.body.day
=======
    teacherName: req.body.teacherName,
    location: req.body.location,
    timeDuration: req.body.timeDuration,
    timeStart: req.body.timeStart
>>>>>>> kornel
  });
  lesson
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created lesson successfully",
        createdLesson: {
          name: result.name,
          price: result.price,
          teacherName: result.teacherName,
          location: result.location,
          timeDuration: result.timeDuration,
          timeStart: result.timeStart,
          day: result.day,
          _id: result._id,
          request: {
            type: "GET",
            url: "http://localhost:3000/lessons/" + result._id
          }
        }
      });
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({
        error: error
      });
    });
});

router.delete("/:lessonId", chechAuth, (req, res, next) => {
  const id = req.params.lessonId;
  Lesson.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "Lesson deleted"
      });
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({
        error: error
      });
    });
});
router.patch("/:lessonId", chechAuth, (req, res, next) => {
  const id = req.params.lessonId;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  Lesson.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      console.log(result);
      res.status(200).json({
        message: "Lesson updated",
        request: {
          type: "GET",
          url: "http://localhost:3000/lessons/" + result._id
        }
      });
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({
        error: error
      });
    });
});

module.exports = router;
