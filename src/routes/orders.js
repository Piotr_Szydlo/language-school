const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

const Order = require("../models/order");
const Lesson = require("../models/lesson");
const chechAuth = require("../middleware/check-auth");
const User = require("../models/user");

router.get("/", chechAuth, (req, res, next) => {
  Order.find()
    .select("lesson quantity _id")
    .populate("lesson", "name")
    .exec()
    .then(docs => {
      res.status(200).json({
        count: docs.length,
        orders: docs.map(doc => {
          return {
            _id: doc._id,
            lesson: doc.lesson,
            quantity: doc.quantity,
            request: {
              type: "GET",
              url: "http://localhost:3000/orders/" + doc._id
            }
          };
        })
      });
    })
    .catch(error => {
      res.status(500).json({
        error: error
      });
    });
});

router.post("/", chechAuth, (req, res, next) => {
  Lesson.findById(req.body.lessonId)
    .then(lesson => {
      if (!lesson) {
        return res.status(404).json({
          message: "Lesson not found"
        });
      }

      User.findOneAndUpdate(
        { _id: req.body.userId },
        { $push: { lessons: lesson._id } },
        function(error, success) {
          if (error) {
            console.log(error);
          } else {
            console.log(success);
          }
        }
      );

      const order = new Order({
        _id: new mongoose.Types.ObjectId(),
        quantity: req.body.quantity,
        lesson: req.body.lessonId,
        userId: req.body.userId
      });

      return order.save();
    })

    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Order stored",
        createdOrder: {
          _id: result._id,
          lesson: result.lesson,
          quantity: result.quantity,
          userId: result.userId
        },
        request: {
          type: "Get",
          url: "http://localhost:3000/orders/" + result._id
        }
      });
    })

    .catch(error => {
      console.log(error);
      res.status(500).json({
        error: error
      });
    });
});

router.delete("/:userId", (req, res, next) => {
  User.remove({ _id: req.params.userId })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "user deleted"
      });
    })
    .catch(error => {
      res.status(500).json({
        error: error
      });
    });
});
router.get("/orderId", chechAuth, (req, res, next) => {
  Order.findById(req.params.orderId)
    .populate("lesson")
    .exec()
    .then(order => {
      if (!order) {
        return res.status(404).json({
          message: "Order not found"
        });
      }
      res.status(200).json({
        order: order,
        request: {
          type: "GET",
          url: "http://localhost3000/orders"
        }
      });
    })
    .catch(error => {
      res.status(500).json({
        error: error
      });
    });
});
router.delete("/:orderId", chechAuth, (req, res, next) => {
  Order.remove({ _id: req.params.orderId })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "Order deleted"
      });
    })
    .catch(error => {
      res.status(500).json({
        error: error
      });
    });
});
module.exports = router;
