const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  lesson: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Lesson",
    required: true
  },
  quantity: { type: Number, default: 1 },
  userId: mongoose.Schema.Types.ObjectId
});

module.exports = mongoose.model("Order", orderSchema);
