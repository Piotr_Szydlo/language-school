const mongoose = require("mongoose");

const lessonSchema =new  mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, require: true },
  teacherName: { type: String, require: true },
  price: { type: Number, require: true },
  location: { type: String, require: true },
  timeDuration: { type: Number, require: true },
  timeStart: { type: Number, require: true },
  day: { type: String, require: true }
});

module.exports = mongoose.model("Lesson", lessonSchema);
